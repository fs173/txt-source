因為活捉黃金蛇順利完成，所以正在返回卡魯村。

乘著哈庫下山的時候，途中有三體狼型魔物突然襲擊了過來。

『讓我來』

嘎卟嘎卟嘎卟───，在電光火石的撕咬下，三體魔物在數秒內便被反殺了。


「真不愧是你，很強啊」
『不不，像野獸一樣的爪牙姑且還是有的』
「狐火，好像也能用的吧」
『在一定範圍內，可以讓對手燒起來。也有不起效果的對手就是了』
「毛也能飛出去，技能真多啊─」
『我可是有很多秘密的狐狸喲。哼哼』

在互相就能力、情報進行交換的期間到達了卡魯村。呼的放下肩上貨物⋯⋯的一瞬間。

『樣子，好像有些奇怪？』
「⋯⋯去裡面看看吧」

水缸破裂，鋤頭倒落地面，很是奇怪。急忙趕到廣場，便看見村民們慌慌張張的聚集在一起。

「村長，發生了什麼事？」
「哦──阿克斯大人。大約一個小時前，有盜賊突然襲擊了過來」
「怎麼會這樣⋯⋯」

仔細看的話，有很多人受了傷。
其中也有人受了致命傷。可惡。

「村子裡不是雇傭了保鏢嗎？」
「全員都被殺了⋯⋯帶頭的男人，是個難以置信的殺人老手。」
「琳怎麼不在。難道被帶走了嗎？」
「就是那個難道。年輕的女孩子們全都被帶走了。」
「往哪個方向去了？」
「去了南邊。能夠借用您的力量嗎？」
「請不要問這樣的問題。這裡是養育了瑪麗的村子。即使是用生命交換，也要救回來」

我走近已經去世的保鏢，為他們祈求冥福。

「絶對要救出來！」
『嗥─────』

哈庫高昂的遠吠著，僅僅數秒便進入了最高速度。我為了不被甩下去，而緊緊的抓住白色的毛髮。

───

世界上，像這個村子裡的慘案每天都在不斷發生著。魔王，魔族，並不是邪惡。不如說真正的邪惡寄宿在人們的心裡。人類之間，貴族和平民，親人之間的爭鬥。

有時候在勇者成為友方之前都在進行戰爭。

我，也已經厭煩了這樣的世界。
我的家族也在旅行的途中全都被殺了。

在被選為勇者的時候，曾在神的面前發誓要改變這世界。
但現實卻很殘酷。只是變強是改變不了什麼的。人的慾望和野心是不會輕易消失的。世界也不會改變。

即使這樣，我也────

『找到了喲！就是那個集團沒錯』
「──我，不記得連內心也 一起丟掉了！」

在前方走著的事數十隻馬匹，還有複數馬車。
騎乘在馬上的是全副武裝的盜賊集團。


「頭兒！是追擊的人，肯定是魔物使」
「全員停下」

敵人最多才３０人，但武具的質量卻很精良⋯⋯

我從哈庫的身上下來，向馬車的載貨台送去視線。看來是被毆打了，村裡的女性們的臉都腫了。而琳這是倒在一旁，並且痛苦著喘著氣。


「對女性揮舞了暴力麼？」

像是領頭的光頭男睥睨著我。

「個性強的人有很多，這是為了讓她們沉默」
「即使這樣也做過頭了」
「倒下是因為抵抗太激烈了。還沒死呢」

不需要慈悲，我拔出劍並架好。

「為什麼瞄準了村子？你們不是普通的盜賊吧」
「真敏銳啊。反正都會殺掉就告訴你好了，俺們是傭兵團。被命令進行女人的籌措」
「僱主是誰？」
「那樣會違約的────你們，全去對付那條大狗。」
『太失禮了，人家是狐狸啦！』

哈庫用身體將數名盜賊給撞飛了出去。當然對手也不只是膽怯，一名盜賊拿劍向哈庫的身體突進了過去。

「沒問題嗎！？」
『請放心，已經將毛硬質化了。雜魚就請交給我』

在離我遠的地方由哈庫做盜賊們的對手。而我這是和頭領面對面一決勝負。

「手下全員，那個怪物有能贏得理由麼？」
「那當然是哈庫那邊比較強啊」
「恰恰相反。和等級更低的友方一起戰鬥，只會成為絆腳石而已」

那樣的話，最初開始就一個人行動不是更好嗎。


「你也挺強的嘛。但是，是絶對贏不了俺的。因為俺的力量────可是勇者直傳的啊。」

兩手各握著三把刀，並投擲過來的頭領。

描繪著零零散散的軌跡朝我精確的狙擊過來。被我用劍全部彈開。原來如此，看來是擁有罕見的技能。但，就這種程度我可不覺得這是勇者直傳。

「可不要想多餘的事」

撞擊的金屬聲。我用劍架住了緊緊咬住我的軍刀。
腕力並不沒有多大。便從容不迫的壓回去，再轉成袈裟斬。存在力量差的話就該靠這個解決。


「什麼⋯⋯」
「呼，好險─」

刀刃只是摩擦了一下便結束了。這個男人，在我行動之前沒有移動一步嗎⋯⋯⋯

再次，劍戟重疊。第五回劍再次相撞的時候，這邊的刀刃折斷了。

「這把軍刀可是破壊系的魔道具呢。就這樣全部結束吧，去死吧。」

在軍刀貫穿我的心臟之前，用『影縫』沉入自己腳下。在那傢伙背後影子的位置，我便從那裡登場，然後往後腦勺的部位毆打過去。

「好險！？」
「不行，麼」

和先前一樣，並不是靠反應避開的。而且還更快。就像是知道了即將來臨的未來一樣。

「呼──真險⋯⋯剛剛的事暗魔法嗎？」
「你真是好眼力。不是氣息察覺系的。」
「僅，僅僅是這樣，就能明白到這種程度嗎⋯⋯」
「結合勇者發言來看，是否也被神眼勇者賦予了預視未來的能力呢」

頭領目瞪口呆，接著咽了口口水。那樣的反應告訴了我那是正確答案。

不僅限於勇者，世上也有能低風險地將自己的一部分力量分給別人的特異體質，很是麻煩。


「這樣的話，僱主也是勇者對吧」
「這可不對了。話說，簡直動不了啊。我既受不到攻擊，你也沒有劍」

劍的話有的啦。

「黑暗啊，請像劍一般顯現吧『冥黑五失劍』」

我握住操縱黑暗形成的劍。只給我一人，只有我一人能握住的漆黑的武器。
刻不容緩的，隨意揮舞著這個。

「話說全部都看見了啊！」
「就算全看見了，也沒關係的啦」
「哈⋯⋯？」

黑劍穿過防禦用的軍刀輕而易舉的向頭領斬了過去。

嘛，雖說是斬擊，但無法造成傷害。相反的──

「欸？哈？怎麼？明明是白天，怎麼突然變暗了⋯⋯」
「看來好像失去視力了呢」
「又不是被砍了眼睛！？」
「本來，只要被五失劍斬過一次就能奪走全部五感。但現在的我，好像只能奪走一個感覺。」

而且好像還是隨機的，可惜了。何況，對這次的對手來說是十分足夠了。

「在這之上不已經是白費力氣了嗎？老實一點吧」
「⋯⋯噢，已經很老實了」

頭領絶望的嘆著氣，反向手持軍刀。

「等等────，⋯⋯太遲了嗎」

頭領將自己的心臟貫穿自殺了。著已經見過很多次了。偶爾也有害怕拷問而這樣的人。
我的心裡雖然抱有一絲虛無感，但也立刻開始確認後背。


『這邊已經結束了喲』
「真不愧是哈庫。接下來」

我登上貨台，抱起倒下的琳。琳的臉上還留有黑色的血漬，腹部等地也受到了相當的損傷。


「哈、哈、西恩⋯⋯」
「不用說話也可以喲」
『暗袋』⋯⋯看來能用。這是魔法的一種，能在特殊的亞空間裡保存物品。

立刻取出最高級的藥水。
暗袋裡只有這一個。因為旅途中取得的東西幾乎都寄存在同伴身上了。

「這是頂級藥水，喝了會好些」
「那，那樣的，我這種人」
「正因為是你才用的。來」

液體從她的嘴裡流入。藥效是即效性的。不到３０秒琳就痊癒了。

「這不是十棟房子才買得起的藥嗎」
「才十棟房子，不是很便宜嗎？」
「⋯⋯為什麼啊。我明明，老是對你說些過分的話⋯⋯」
「沒關係的。總之現在要好好休息才行」

琳是位纖細的女性，即使在逞強也應該很害怕。肩膀現在也在微微顫抖，就是這樣。
讓大家冷靜下來從貨台上下來的時候，從後背傳來很小聲的一句話。


對不起。

我回過頭，，微笑著豎起大拇指。


◇　◆　◇

翌日、卡魯村も落ち著いたので、俺は晝前に發つことにした。
村長や村人達が入り口まで見送りにきてくれてとても嬉しかったな。

「感謝の言葉もありません。これは、ほんの少しですが」

村長から寶石やらが詰まった袋を贈呈されそうだったので、俺は遠慮する。

「それは復興に使ってください。というか俺、光物は苦手なので。⋯⋯元闇勇者だけにね」
「⋯⋯⋯⋯」
『ぷぷぷ』

おかしいな、哈庫にしかウケてないぞ。微妙に恥ずかしい。

「ごっ、ごほん。殘黨はいないようですから安心してください。妙な氣配や樣子を感じましたら、戻ってきますので安心してください」
「何から何まで、誠にありがとうございます」
「あと、琳にもよろしくお傳えください」
「我々が呼んできますよ！」
「いえ、いいんです。もう出發しますので」

皆に手を大きく振って、お別れの挨拶とする。
少し進んだところで哈庫に止まってもらい、振り返る。

「これで、あの村も見納めか」
『西恩桑。その件についてお話があります』
「俺の方も、少し相談したいことがあってさ」

內心吐露しようとして、一時中止する。俺の名前を叫びながら、近づいてくる女性がいたからだ。

「アックスーーッ！待ってえええ！」
「琳！？」

息を切らしてまで追ってきてくれたことに俺は胸が高鳴った。

「わざわざ、見送りに？」
「言いたいこと、まだ言えてないから」

琳は息を整えると、俺の目を見據えた。

「本當はあたし、全部知ってたの。⋯⋯あの日、お姉醬が仕事に行けって、頼んだんでしょ？」
「だとしても、俺の罪は消えない」
「罪なんかじゃない！そうじゃないのよ！あんたには何の責任もない。いつも村のために、お姉醬のために動いてた。誰かのために立派に勇者やってた！」

呀咧ていただろうか？　世界に對する俺の力は無力で、本當はいつも自信がなかった。
俯いて地面を見つめる俺の肩に、琳の兩手がかかる。

「あたしが子供だったのよ。誰かのせいにしないと悲しくて、やってられなくて⋯⋯⋯弱かったの⋯⋯⋯でももう、やめるから。もう、そういうのやめるから」
「琳⋯⋯」
「だから、アンタもこれからは自分のために生きてよ！勇者やめたんでしょ？　じゃあ、これからは自分のために生きて。もう、十分だよ。あんたに救われた人、どれだけいるのよ」
「⋯⋯」
「辛いことあっても死んじゃダメだよ。嚯啦、これあげるから」

渡されたものに俺は驚く。酒漬けにされた黃金蛇だったからだ。

「すごく美味しいのよ。都市に行く途中で、飲んで」
「⋯⋯ありがたく、いただくよ」
「また村に寄って。待ってるわ」
「また、必ず」

最後にハグをして、笑顏で琳と別れた。
心の奧が、溫かい。冷たく凍っていた氣持ちが、靜かに融けていく。
長く氣になっていた胸のモヤモヤが、取り払われるのを強く感じる。

『わーっ、思い出しましたーっ』
「うわっととと」

哈庫が突拍子もなく叫ぶので、危うく背中から轉げ落ちそうになる。

『これは失禮。でも大事なことを思い出しました。黃金蛇の酒漬けについてです！』
「聞きたいなそれ」
『二百年くらい前、ある男の逸話を聞きました。男が人生に疲れ死を選ぼうとした時、その蛇酒を妻に飲まされたそうです。すると男は急に明るくなり、ついには死ぬのを忘れたと言います』

俺は黃金蛇を見つめる。
つまり、琳には全てお見通しだった、ってことだろうか。

「なぁ哈庫、一つ相談したいんだ」
『どうぞどうぞ』
「一緒に死ぬ話だが、もう少し延期してもいいかな？　まだ見えてない世界が、もしかしたらあるのかなって⋯⋯」
『先ほど、私もそれを告げようとしたんです。もう少し、生を樂しむのも惡くないですよねっ』

ウインクをする哈庫に、俺は滿面の笑顏を返す。

ぎゅぅぅぐるるるるる
るるるるるるるるるる
るるるるるるるるるる

「哈庫桑や、さすがに鳴りすぎじゃないかい」
『おっはずかしい限りです～、獲物とってきますねーっ！！』

尻尾を振りながらどんどん遠くなっていく哈庫を俺は笑いながら見送る。
不思議な、氣分だった。

ほんの一週間前までは──

闇之勇者をやっていた。

ほんの數日前までは──

死を望んでいた。

でも今は──

また前に歩き出そうとしている。

「第二の人生、ってやつなのかな」

白狐と歩む放浪の旅か。
そんなのも、惡くないかもな。