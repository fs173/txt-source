「那麼，誠一，要加油吶！」
「會在觀眾席上為你應援的喔！」
「哦！」

俺──柊誠一，在回應了莎莉婭與阿爾的聲援之語後，前往露露妮所在的小馬棚。
然後今天是要在王都杯上出場的日子，那個甚至可以說是一種無謀的挑戰。
糟了，事到如今要怎麼辦啊。
昨天，與梅茵還有克雷道別之後，接著就回到旅店的俺，向莎莉婭與阿爾她們介紹了露露妮。
因為梅茵那時也沒什麼問題，想著讓露露妮與莎莉婭她們接觸應該也不會發動攻擊吧，結果和預料的一樣，露露妮也很溫馴地對待莎莉婭和阿爾。
因此，露露妮與莎莉婭她們沒有花很多的時間關係就變得挺融洽的，原本莎莉婭就是猩猩，但不知為什麼卻可以跟俺一樣與露露妮互相理解。
......為什麼大猩猩能與驢馬互相理解，這個問題也不是這個當下要考慮的。
在那之後，把今天會在王都杯中出場的原委也傳達過了，雖然兩人一開始是面露驚色，但馬上就開始為俺加油。
即便如此，阿爾是說了會在觀眾席上給俺加油......
但是觀眾席是在哪裡啊？
那個先暫且不論，因為兩個人都為俺應援，可以的話真想贏啊。
因為露露妮對她的速度之快說得是那般自信滿滿，那就算優勝沒什麼可能但某種程度的名次也可以拿到的，是這樣的吧？這樣思考著。
只是，為什麼呢，就只有不好的預感，這樣的俺不就很奇怪嗎？
不對，這本來就是難於登天的事，因此這就是在挑戰那種被斷言是不可能的事。
現在真的就什麼討厭的預感也沒有啊
邊走邊想著這些事情，不知不覺已經到達露露妮所在的小馬棚。
一進到裡面，就看到其他冒險者和商人他們的馬，在這城一起進食牧草一起生活。
一邊遠遠瞄著其他馬的樣子，一邊前往露露妮的所在地。
然後，露露妮察覺到俺的靠近，用對於驢馬來說甚是浪費的凜然面容來跟俺打招呼。

『主人，早上好！今天是絕好的騎馬天氣呢！』
「早啊！雖然什麼是騎馬的大好天氣俺是不知道啦。今天也請多關照！」
『請儘管交給我吧。我露露妮為了守護主人的名譽，必定會助主人奪取優勝的說！』
「真心話呢？」
『為了巴哈姆特！』
「根本就不是為了奪冠啊！」

因為巴哈姆特是第五名次獲勝者的獎品，要是取得了優勝就得不到了。
在競賽開始之前，好像餘下的從第四名到冠軍的獎品都會被公佈出來......
如果想要取得優勝的話，優勝獎品最起碼也得是能吸引露露妮的食物才行啊。
不過在此之上，露露妮是驢馬，優勝是絕望般的不可能。

「嘛，算了！已經沒有那樣的美國時間了，走吧？」
『是！』

和露露妮從小馬棚裡出來後，俺移步到王都杯的起跑地點，即是王都特魯貝魯城門的那個盤查處的門口那一帶。
終於到達那個門口，而作為參加者的冒險者們，都牽著各式各樣的馬聚集在那裡。

「哦～～好厲害！除了俺知道的馬以外，還有這麼多其他種類的馬在啊......馬鹿也在啊，這沒問題嗎？」

當俺環視著周邊的馬，一個人在自言自語的時候，聽到有聲音從身後傳來了。

「誠一！」
「誠一先生！」
「嗯？」

回過頭去看到的是，身著豪華衣裳的克雷，以及穿著襯衫和長裙的梅茵往這邊過來了。

「是克雷和梅茵啊，兩位這是怎麼了？」
「我這是為了來見證你以及驢馬到底能努力到何種程度啊。」
「我是，因為誠一先生說了要為了給予我勇氣而去參賽......覺得沒有理由不來應援，所以就來了。」
「這樣啊。嘛，為了梅茵也會努力的！」

戴著擋風帽的俺帶著苦笑告訴她，梅茵則是面露抱歉之色說道。

「誠一先生，我已經決定會在繪畫大會裡出場了。雖然，要畫點什麼還沒決定......因此，請誠一先生不用在意我這邊，請按自已的想法去進行比賽。還有就是，誠一先生的目標是第五名的獎品巴哈姆特吧？」
「差不多吧。」

那主要是露露妮的目標。

「那樣的話，請不必考慮取得優勝，請努力地瞄準自己的目標名次！這邊也會給你加油的！」
「謝謝！」

可是，為什麼呢。
總感覺這個的言外之意說的是不可能奪冠。
想著這樣的事的時候，俺突然有個地方在意起來了。

「這麼說來，那些觀戰的人們，要怎樣觀看俺們的比賽呢？」

王都杯的競賽只是單純地圍繞著城牆跑一圈。
但是，包圍著特魯貝魯的整個街鎮的城牆，自然是超乎想像的長。
大概是像地球的月臺一樣，會在各個網站那裡應援吧？
阿爾和莎莉婭好像是要在觀眾席上觀戰的樣子，但......
面對試著假設各種猜想的俺，克雷露出一臉愕然的簡直就在說你這傢夥在說什麼的表情並進行說明。

「也沒有其他怎麼辦的，肯定是使用『魔力投影機』的吧。」
「......什麼啊，那個？」

不禁用零散的話問道，而這次則是由梅茵來回答。

「那個，稱為魔力投影機的是一種魔法道具，通過那些憑藉魔法在空中飄浮起來的無數『魔導相機』，然後將拍攝到的影像在空中投影出來，無論誰都可以看到遠在其他地方的影像的物品。而作為魔力投影機動力的魔力，是由宮廷魔術師那邊所供給的呢。」

魔法的力量好厲害！
在看到回復魔法的時候就在想，讓人驚奇地感覺不到科學的必要性，魔法已經是無所不能了吧？
甚至連讓死人復活也可以做到。

「大致上是設置有觀戰場所的，但是基本上通過被投影出來的影像就可以讓王都全體人員都能看得到，因此在各個地方都能觀戰的。而我和克雷先生，則是打算在觀戰場所那裡看著被投影出來的影像來為你加油。」
「原來是這樣啊......。」

通過梅茵的解說，俺理解了。
儘管是比較安全沒錯，但在城牆外面畢竟也有魔物在啊。
在那樣的地方應該是不能好好觀戰的吧。

「啊！差不多再不出發的話，到那邊就會沒有位置了呢......。」
「對啊......誠一，我和梅茵要動身去觀戰場地了。真期待你的活躍啊！」
「請努力加油吶！」

最後，在俺回以兩人問候之前，就已經向觀戰場地出發了。

「嘛......總之加油吧。」

如此自言自語的同時，俺牽著露露妮往起跑地點那邊過去了。

◇◇◇◆◆◆◇◇◇

『好了，要開始囉！每年都會贈與豪華獎品的王都杯！而在幸擔任這個王都杯司儀的是騎士團『聖劍的女武神』所屬的羅娜‧奇裡薩茲！雖然時間很短，還請各位多多關照！』

女性的聲音響徹了整個街鎮。
然後，在空中投影出了集中在起跑線的這次王都杯的每一位出場者。
......雖然再也提不起幹勁來一一吐槽，但讓這個司儀的聲音得以傳播的技術也是魔法什麼的吧。
話說科學完全就不需要了吶？
跟精神疲憊的俺恰恰相反，周圍的的參加者和司儀的聲音讓人感覺熱情高漲。

『那麼，在說明規則以及起跑之前......我想要先公佈一下各位現在所在意的，第一名到第四名的獎品！』

司儀這麼宣佈的那個瞬間，周圍的人陷入了高度的沸騰之中。

「嗚噢噢噢噢噢噢噢！」
「這次會是怎樣強勁的獎品啊！？」
「絕對不會讓出優勝的！」
「勝利在向我招手啊啊啊啊啊啊啊！」

四周的人好可怕......！
周圍逐節高漲的呼喊聲把俺的身體震得下一跳。
然後，露露妮以不輸給四周這樣的熱情，沉靜地散發著鬥氣。

『巴哈姆特、巴哈姆特、巴哈姆特、巴哈姆特、巴哈姆特、巴哈姆特......』
「夠了！喂，你這孩子！」

這不就只是在想著食物嗎！不對，雖然俺也想試一下味道就是了！
就在俺一個人處於萎靡不振狀態的時候，聽到了司儀愉悅的聲音。

『嗯～～，真是好棒的狂熱氣氛呢！那麼這邊也不會輸給這有著種熱情大家，這次的獎品也非常的棒喲！而且，這次的優勝獎品變成了豈今為止都沒有過的新式獎品啊！』

新式獎品？

那到底是什麼啊......
對於司儀的話，俺只是疑惑不解，但露露妮則表現出在俺之上的強烈反應。

『居然是，新，新式獎品！？難，難道說......是超越巴哈姆特的食材嗎！？』
「暫時給俺拋開吃的話題啊！」
『庫～～！雖然巴哈姆特不管怎樣都想要，但要是優勝獎品也是食材的話......我，到底要怎麼辦才好啊！？』
「聽俺說話啊啊啊啊啊啊啊啊啊！」

俺的吐槽也略顯空虛，而露露妮則開始一個人在煩惱起來了。
最後真的會哭喔。
正當俺看到露露妮對食物的執著而欲哭無淚之時，司儀那邊在公佈獎品。

『那麼，首先是從第四名的獎品開始！第四名的獎品是......我等『聖劍的女武神』的團長露易絲大人在某個地下城得到的，傳說級的武器『海割』！把這個武器作為獎品的理由嘛，單純只是因為我等並不是使用斧頭來戰鬥的騎士。嘛，即使傳說級的武器落入他國之手，我們也是不會輸的！』

一抬頭望向上空就可以看得到，到剛才為止還映照著俺們參加者的影像變成了有著綺麗的青色刃口的大斧頭。
恐怕那個就是叫作海割的武器吧。
話說，第四名的獎品是傳說級的武器啊。
說真的優勝獎品到底會是什麼啊？
而且，那麼簡單就可以把傳說級武器當作獎品，真的就如『聖劍的女武神』之名一樣，這個應該騎士團很強吧。
之前伽魯斯也說過，好像這個國家有兩個超強的騎士呢。

『下一個是第三名！第三名的獎品......同樣是，露易絲大人在地下城得到的傳說級防具『破邪之護臂』！把這個防具作為獎品的理由，跟第四名的獎品海割的理由是一樣的！都拿去吧，你們這群小偷！』

「「「嗚噢噢噢噢噢噢噢！」」」

對司儀的話產生反應，四周圍大聲呼叫起來。
映射不知什麼時候從海割變換成白銀色的護臂，那個應該就是所說的破邪之護臂吧。
可是......真厲害啊。
俺想只要是冒險者，不管是第四名的海割還是第三名的破邪之護臂都想要，因此全員都會努力奮鬥的吧。
本來，因為這個王都杯，就是冒險者都嚮往的大會，因此理所當然不會是什麼普通的獎品。

『接下來是第二名！第二名的獎品是......收錄了我等的團長，露易絲大人各種姿態的貴重寫真！怎麼樣？是不是很想要啊！』

誰會要啊！？
為什麼這裡會是寫真啊！？

『這個可是付出了慘重的代價，用『魔導式相機』偷偷拍下的珍貴逸品喔！嘛，雖然這個大會之後，我肯定是會被發怒的！』

笨蛋啊！
偷偷拍照也好，慘重的代價也罷......
真的是白癡一樣的發言啊！
而且，既然都知道會被批，為什麼還要那樣做啊！？
而影像也由先前的護臂變成的打著馬賽克的寫真。
這馬賽克打得......何等浪費的技術啊！
為什麼啊！？
明明第四位和第三位的都那麼厲害！
難道還真的是寫真！？
誰會想要啊，那種東東！

「絕對要拿到手啊啊啊啊啊啊啊啊啊啊啊！」
「姐姐大人的寫真是人家的喲喲喲喲喲喲喲喲噢！」
「嗚謔噢噢噢噢噢噢噢！露易絲大人萬歲！」

比傳說級武器的熱情還要高漲......
什麼鬼啊！？
這個國家真心糟糕......！
更讓人震驚的是，不僅是男人，就連女性也想要露易絲大人的寫真，全員都望眼欲穿，佈滿血絲的眼睛都紅透了，真心太可怕了......好想回去啊。
當俺還在不禁抱頭痛哭之時，司儀終於要開始公佈優勝獎品了。

『來了來了，終於要來了喔！現在發佈優勝獎品！』

參賽者們再一次因為司儀的話而熱情高漲。

『對於榮獲第一名的光榮選手......居然是，賦予其與我等『聖劍的女武神』一起度過一日的權利！』

才不會要啊！
把第三位還有第四位的感動還來啊白癡！
在內心進行著盛大吐槽的時候，周邊的參加者們，直到剛才為止的狂熱都跟騙人一樣，定格無語了。
那是當然的吧！優勝獎品居然是那種不明不白的權利什麼的喔！？
得到那種東西，哪會有高興的傢夥──

「「「──優勝是俺的啊啊啊啊啊啊啊啊啊啊啊！」」」
「「「──優勝是人家的喲喲喲喲喲喲喲喲喲喲！」」」

誰來給俺救救這群傢夥啊啊啊啊啊啊啊啊！！
參加者們，突破剛才的沉默，用最高的熱情喊叫。

『很棒的狂熱氣氛！嘛，如果被賦予能與露易絲大人直接會面的機會的話，當然是誰都夢寐以求的吶！趁這種興奮還沒有冷卻之際，就讓我來對規則進行說明啊！』

司儀對這種狀況是打從心底感到高興地笑著，並開始說明。

『雖說是規則，但複雜難懂的可是一條都沒有！總之，就只是環繞著這個包圍王都特魯貝魯整個街鎮的城牆跑一圈而已！只是呢，對他人進行妨害，以及做出明顯的危險行為和犯規行為的那一刻，即時喪失比賽資格！也就是說，如果有想要的東西的話，就給我堂堂正正地贏到手呀呀呀呀呀呀呀呀啊！』

「「「哦哦哦哦哦哦哦哦哦哦哦哦！」」」

等聽到參加者們的叫聲稍微平息下來後，司儀繼續補充道。

『還有就是，本來王都杯的參加者主要由冒險者組成，而一般人是不能參加的，因此就只能在一邊遠遠地欣賞！所以，從這一屆開始，打賭哪位參賽者能榮獲第一名的『賽馬』活動就此開啟了！這樣一來，作為一般觀眾的各位，就不只是能觀賞到賽事，要是運氣好的話還能變得更加的高興，變成了這樣的大會！對了對了，關於賭注金額，因為要防止大會之後出現寄生蟲和破產人士，所以將一萬G定為封頂的金額！請各位踴躍地參加吧！』

賽馬！？
搞出賽馬這種事來！？
但是也確實是這樣，只是看著的話可能會挺無聊的......
到底這種賽馬形式的活動最終會不會演變成好結果，俺是不清楚啦。
嘛，都說了是從這一屆開始，大概也有試驗的意圖在裡面吧。
想著這種事的時候，被投影在空中的影像變換了，參加者的名字，和他所乘騎的馬的種類，還有倍率等都顯示在上面。

『這些就是參加這次的王都杯的選手！果然人氣最高的是，在馬族種類中被譽為最快速的『流星馬（meteorhorse）』與邁克選手的組合吶！邁克選手有著上一屆王都杯優勝獎的實績，將他視為優勝者的最有力候補是有其妥當之處的吧！』

嘿～～
......前優勝選手也在啊。
速度應該超快的吧。

『只是，在這一屆的大會裡面好像也有很多其他給力的選手呢，到底鹿死誰手仍是個未知之數！』

嘛，反正跟俺沒關係啊。
最後露露妮也明白到優勝獎品並不是食材，因此散發出一股絕對要得到第五位的巴哈姆特的氣勢。
司儀持續進行著對賽事的預想和各選手的實力分析，而就在這時。

『......嗯唔！？這，這是怎麼一回事的說！居然，出現倍率120倍這種意想不到的組合！這是真的嗎！？』

......啊咧？
總感覺有種不祥的預感......

『名字是誠一選手，騎的馬是......驢，驢馬馬馬馬馬馬馬馬！？誠一選手，你真的想贏嗎！？』

求別說啊啊啊啊啊啊啊啊！！！
俺自己是最清楚的啦！
用驢馬來挑戰，這種無謀的事！
所以至少......
至少讓俺在一邊靜靜啊！
別再欺負俺了啊啊啊啊啊啊啊！！！
試著環視一下周圍，一望過去就看到其他的參賽者用溫暖的目光在看著俺。
那種眼神才不想看到啊！

『嘛，嘛，這也挺好的啦！那麼，規則說明和獎品的發表都完了！之後......就只剩競賽了！那麼，因為要開跑了，所以請各位移步到起跑線那邊吧！』

結果俺就一邊被旁人用溫暖的目光護送著，一邊移動到起跑地點。
雖然這是後來聽說的，據說當這個羞恥的影像被王都全體人員看到後，他們隨即瘋狂地爆笑起來。
......真的哪裡都沒有洞嗎？
真心不是開玩笑的，俺都己經開始找起能讓人藏起來的洞了。
