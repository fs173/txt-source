尖銳且規律的金屬聲回蕩在寬廣的地下空間。

拿著十字鎬往牆壁敲，從崩壞的地方撿起掉下的金屬之後，杰羅斯露出滿足的笑容。即使完全沒有倚賴作弊的能力，卻能感應到金屬的所在位置，無可奈何之下只好試著開採看看，好笑的是居然真的挖到礦石了，所以他現在心情很好。

大範圍殲滅魔法「煉獄炎焦滅陣」的熱度融解了金屬，讓蘊含礦石、比重較重的相同種類金屬彼此結合，因此確保了包含稀有金屬在內的大量礦物。

將其提煉後便可作成金屬條，一一收進道具欄裡。

「喔？這個閃耀著七彩光芒的應該是日緋色金吧。居然可以在這裡挖到，真是座優良的礦山哪，咯咯咯咯。」

「為什麼連我都要採礦啊……啊，挖到一個怪怪的黏土塊……」

「那是精金呢。運氣真是太好了，哈哈哈哈！」

「啥？」

克莉絲汀手中拿著賣掉就可以吃喝揮霍幾十年的礦石僵在原地。

杰羅斯很開心地繼續挖礦。偶爾會使用魔法炸毀牆壁，選出品質較好的礦石。他的目的是打造付乾燥機功能的筒倉、冰箱，以及腳踏式打谷機，但他已經挖到超出所需的金屬礦了，所以心中燃起一股這樣說不定要打造自動加熱浴缸也行的野心。

「哎呀～愈挖愈多，真讓我笑得合不攏嘴了呢。Ha～hahahaha！」

「你好像變了個人耶？比起那個，能聽我說一下嗎，這是精金？我記得精金是……」

「米啊……我將以我的手取回稻米。酒、醬油、味噌、味醂，還有酒，等等我吧，我要以我的雙手復興飲食文化～！嗚咿哈哈哈哈哈哈哈哈哈哈！」

「根本沒在聽……你為什麼這麼興奮啊？而且說了兩次酒耶？那很重要嗎？」

對喜歡日本酒的大叔來說確實很重要。

揮舞的十字鎬發出清脆的聲音粉碎熔岩，敲出的碎片彈至四周，同時確實地挖開岩盤。挖掘的速度快速且準確得驚人，看來他也可以當個一流礦工吧。而且毫無疑問會是賺最多的那一個。

順利採到夢寐以求的礦石，又更貼近日本生活一步的杰羅斯滿腦子小確幸。就像在國外想念家鄉料理那樣，無法回去的杰羅斯渴望著故鄉的味道。

那甚至讓他興奮到有些異常的程度。

這異常的興奮態度持續了一段時間，直到他滿足為止前，奇怪的笑聲不斷回蕩在礦場中。現在不管誰說了什麼，他都聽不進去。

而他也因此采到了大量的礦石，多到他根本用不完的程度……

◇　◇　◇　◇

「哎呀，一把年紀了還這樣一頭熱，真是讓你們見笑了。」

「不，這是無所謂啦……但我們要不要快點離開這裡？要是魔物再出現的話……」

「覺得會被我的魔法連累嗎？」

「沒錯……不是啦，是因為我等級太低，沒辦法跟魔物對抗啦！」

「你剛剛一不小心說出真心話了吧？哎，是無所謂啦……」

本來是要來救助克莉絲汀的，但按目前的狀況來看，他們應該是在最底層。

要不是運氣太差，不然一口氣從上面的階層落到最底層這種事基本上不太會發生。然而新生成的迷宮為了維持環境必須收集魔力，所以才會有用來捕捉倒楣祭品的陷阱。

迷宮本身沒有意志，但既然會產生這種設下陷阱的變化，那就應該有一套類似系統的法則吧。不過迷宮形成的原因尚未解開，至今仍是謎團重重。

可是這個世界的人們理所當然地接受了這個狀況。一般民眾之中沒人覺得這有什麼奇怪，只有腦袋僵化的學者才會對為什麼會產生這種現象抱持疑問，並且打算考究個中理由。

「話說回來，那片廣闊的沙地如今已經成了一座火山呢。」

「威力比想像中大多了，以前的規模其實更小的……」

這就是虛擬世界的特效圖片跟現實世界的差距。

重點是在這裡蠢動的蠕虫比杰羅斯弱太多了，而杰羅斯卻以強到過頭的火力燒死了它們。「殲滅者」這別稱可不是浪得虛名。

或許是玩遊戲時留下的習慣，只要發現周遭出現大量敵人，他就會立刻祭出一記威力強大的魔法。而且因為不需要咏唱，只需要魔力而已。

「看起來像是把魔法術式本身給打了出去，但到底是基於什麼原理做到的啊？這很明顯跟一般的魔法不一樣耶！」

「就是說啊～所以才是危險的魔法，但沒想到威力居然這麼離譜……」

「煉獄炎焦滅陣」是以高密度的魔法術式架構出來的魔法。原本應該是要在腦中處理魔法術式，為了引發現象而展開魔法陣。但杰羅斯的魔法卻是把魔法術式的一切當成砲彈般複寫之後打進去。以0跟1建構的高密度魔法術式，恐怕需要無法在人類腦中完成的高速處理，畢竟要在人腦內做出像CPU那樣的高速處理基本上就是不可能的。所以他才會想說把處理程式本身也打包進去，當成砲彈來使用就好。這窮凶惡極的破壞魔法于焉誕生。

在壓縮魔法術式制成的魔法彈上灌注做為引信的魔力後擊出，並在目的地展開魔法術式。高速處理程式將展開設定好的魔法術式，同時從周遭吸取魔力，接著將魔力轉化成莫大的破壞力後，以物理現象的方式呈現出來。

「暴虐無道西風神之進擊」也一樣。這是讓旋風與其周圍的微小物質高速旋轉的粉碎魔法，會將所有有形的物體撕裂磨碎後吸收，增加本身威力的凶暴狂風禁術。如果能在魔法術式中快速處理高密度情報，就不需要在腦內解讀並處理魔法術式了。伊斯特魯魔法學院研究的大範圍殲滅魔法雖然是預設一個人可以獨自處理龐大的魔法術式而打造的，但真要說起來，要快速讀取循環的魔法術式，並將其轉化為物理現象，就幾乎不是人類的大腦能應付得了的工作，人類也無法承受這麼龐大的負荷。

就算真的完成了，也會在為了確認其威力的實驗實證階段導致施術者的腦組織遭到破壞，進而喪命吧。

在原本的世界，若在遊戲中使用還處於試做階段的這種魔法，甚至會造成劇烈頭痛，因而啟動遊戲筐體本身的安全裝置，導致遊戲強制中斷。

這是一個不小心就有可能會讓玩家變成廢人的危險行為。

『沒想到當遊戲內的魔法變成現實，竟然會是這麼危險的玩意啊～威力太誇張，使用的場合也很有限。殲滅戰先不提，這時候還是用普通的標準魔法比較好吧？畢竟要是搞出奇怪的傳聞也很麻煩，更重要的是我不想被國家盯上啊。話說回來，這種程式竟然可以在遊戲內運作啊，這不管怎麼想都是BUG吧……那個世界果然也是什麼異世界嗎？』

來到這個世界之後，他思考過好幾次那款線上遊戲「Sword and Sorcery」的異常性。

身為主系統中樞的高速處理電腦，通稱「BABEL」的這玩意，原本是由國防部打造，想用在國防上的情報管制系統。面對愈來愈惡質的網路犯罪與來自外界的違法連線，為了保護個人與國家的機密情報而做，但途中因為預算方面的問題被拿出來審視，計畫本身中途腰斬。這個系統也無法繼續運作下去，於是被民間公司買走了。

而買下了這個系統的公司，在那之後大張旗鼓地對外宣傳的產品，就是這款網路遊戲「Sword and Sorcery」。

遊戲名稱雖然老套，但透過購買市售的專用筐體連線後，便可自由地在廣大的電腦空間裡冒險。從參與的玩家人數來看，應該帶來了規模不小的經濟效果。

但明明是那麼大的企業，到了這個世界後卻想不起那間公司的名字。

『說起來，為什麼到了現在才在想這個？只能認為自己是轉生到異世界之後，某種限制被打開了。而且開發那款遊戲的公司名稱……就是想不起來。不，或許認定這開發公司打從一開始就不存在會比較好嗎？總覺得像是那種寫到爛的輕小說劇情哪。』

反覆思考過好幾次原本的世界那異常的現實。就算與現況比對下來，也會覺得兩個世界之間的共通點很多，然後又都不太一樣。法則有些許不同。

思考逐漸陷入深淵。

「…斯……杰羅斯先生！」

「啊！怎、怎麼了嗎？」

「這是我要說的話。我一直在說差不多該離開這裡了，你卻擺出可怕的表情陷入沉思。到底是怎麼了？」

杰羅斯回過神，發現這裡是充滿融解岩石的世界，他這才想起自己現在所處的狀況。

「喔，說得也是，採掘的工作也結束了，差不多該回上面了。」

「要上去是無妨，但我不會很礙手礙腳嗎？」

「礙手礙腳？為什麼？」

「因為……我很弱。」

看著克莉絲汀垂著頭，杰羅斯困擾地搔了搔一頭亂髮。

他們現在在迷宮的最底層。若要往上走，肯定會遭遇魔物，進入戰鬥的可能性很高吧。

以她的實力來看，要在這一層前進確實危險。

杰羅斯正在思索此事的時候，不經意地抬頭看了看上方。因為崩塌而出現的另一個樓層吸引了他的目光。

「有偷懶的方法喔，只要穿過那裡就可以了。」

「咦？可是要怎麼到那麼高的地方……」

杰羅斯手指的天花板處可以看到另一個樓層的通路。也就是說……

「該、該不會要飛上天……」

「除此之外還有其他方法嗎？」

「我、我剛剛才從斷崖上掉下去耶……？」

「你只要牢牢抓緊就不會有事。安啦～你可以數數看上面有幾個污點，這樣很快就會結束囉。很快的……」

「你不覺得你的說法不太對勁嗎？」

對從懸崖上摔下來的她來說，高處已經成了她的心裡陰影。

她的臉上也略略抽搐，並對以飛行魔法移動一事面露難色。

「難道你覺得跟你同行的那幫人可以到得了這裡嗎？姑且不論這邊，每一層樓都有魔物出沒喔？」

「但、但是，那麼高的地方……」

「你不要掙扎的話很快就會結束了，你不要掙扎的話。」

飛行魔法畢竟違反物理法則。要以自身魔力製造相斥力場，而且要用自然界的魔力補強推進力，所以魔力的消耗速度相當快。而且頂多只能負擔一個人的體重，若要再增加重量，就會因為增加的負擔而更加消耗魔力。

「就、就算順利抵達上面的樓層好了，但我還是算不上戰力啊！」

「到時候我就會借你秘密武器。如果是這邊的魔物，一擊就能搞定嘍。嘿嘿嘿……」

那是杰羅斯跟伙伴一起做出來的魔改武器。蘊含過於強大的破壞力，能讓大多數的雜魚一擊斃命。

若給杰羅斯用，那毫無疑問會是屠殺兵器；但要是讓克莉絲汀拿著就只會發揮某種程度的威力吧。

「有沒有其他選項……」

「沒有。你在這裡磨蹭的時候，隨行的騎士們說不定正奮不顧身地拚上小命呢～希望他們不要死掉啊。」

「唔唔……」

還無法下定決心的她抬頭看看上方，畏畏縮縮的想逃避。無可奈何，杰羅斯只能強行抱住她。她的身體觸感意外地柔軟。

「『暗鴉之翼』。」

「咿呀啊啊啊啊啊啊啊啊啊！」

杰羅斯無視發出慘叫的她，徑自高高往上空飛去，而且還是以相當快的速度……

慘叫傳遍了底層的寬廣空間，大叔抱著一個少女快速上升。

他只是想快點回去著手製作道具，但被連累的克莉絲汀實在太可怜了。她後來甚至表示：「天空好可怕……我再也不要飛在天上了……」

大叔只是更加深了高處帶給她的心靈陰影罷了。

而且還沒有自覺，真是太糟糕了。

◇　◇　◇　◇

利用飛行魔法抵達上方樓層的兩人，便從那裡往地面上前進。

大多數的魔物都被杰羅斯給收拾掉了，克莉絲汀只負責跟在他身後。

雖然說自己不成戰力，但她確實很弱。這兩個人現在正在岩場的狹小暗處休息。

幸虧杰羅斯的道具欄裡存有柴薪，他們將柴薪點燃後弄出了營火。

並著手準備略嫌遲來的午餐。

「嗯。是說，克莉絲汀小姐不用魔法嗎？」

聽到杰羅斯這麼開口問，克莉絲汀垂著頭說道。

「我無法使用魔法。好像是沒資質吧，所以儘管我能記住魔法術式，卻無法發動。就算能感受到消耗魔力時的疲勞感，但就是沒資質……」

類似的狀況好像在哪聽過。也就是說，因為魔法術式不完整才導致她無法使用魔法的可能性很高。

「原來如此……那假設你可以用魔法的話，會想學嗎？」

「如果我能用當然會想學，但沒辦法。魔導士們都這樣說了。」

杰羅斯一邊吞雲吐霧，一邊從道具欄中取出幾張魔法紙。

這些都是將魔法學院使用的教科書修改過後的產物，也是瑟雷絲緹娜等人學會的魔法的卷軸。雖然是因為有人找他談販售的生意才實驗性做出的魔法卷軸，但這卷軸裡面並沒有編入消除魔法的術式，任何人都可以使用。

「這是？」

「雖然只是猜測，但我想這個魔法你應該也能用喔？」

「可是我沒資質……」

「現在可是攸關生死的重要時刻，不覺得把能做的事情做一做也沒什麼損失嗎？」

「唔……確實有道理。那麼，我來試試看。」

克莉絲汀按照杰羅斯的指示灌入魔力，展開魔法術式之後，刻劃於腦海中。

完成之後，她低語般吟唱出咒文。

「『燃燒吧，火炬，照亮我的道路。』──『火炬』。」

接著就看到一團小小的火焰在她手掌上燃燒。克莉絲汀驚訝地瞪大雙眼。

「杰、杰羅斯先生！這魔法不像之前我學會的魔法術式那樣會讓身體感到疲倦！」

「那真是太好了。啊，卷軸要還我喔。這要是傳開了可就不妙了。」

「我……竟然能用魔法了……」

「我的學生裡面有個人是因為跟你一樣的理由而無法使用魔法。請你把這魔法當成是近似於古代魔法的東西。」

說穿了就是因為魔法術式不完整而無法發動魔法。

就算沒有成功發動，魔法術式也還是有運作，所以才會感覺到消耗魔力時特有的疲倦感。不，應該說就是因為沒有發動，負荷才更大，而且消耗的魔力量也很多。

因為原因跟自己的學生瑟雷絲緹娜一樣，對應起來也容易得多。

騎士家系出身的克莉絲汀並沒有那麼拘泥於學會使用魔法這件事。但若能使用魔法，她的選項也會變得更多更廣。

雖然她現在還沒察覺這件事……

「那個，可以請問一下嗎？這魔法是杰羅斯先生自創的吧？」

「不，我只是隨意改良了魔法學院的教科書，所以你可以隨便用。若你發生了什麼不測，我對那些隨行的騎士們也很過意不去啊。」

騎士們為了尋找克莉絲汀，肯定來到了下層。要是跟他們會合時克莉絲汀受傷了，那就頭大了。杰羅斯只希望麻煩事愈少愈好。

克莉絲汀急忙記住好幾種魔法。

「那個，有件事情我有點介意，請問那到底是什麼肉？」

她看著正在火上烤串燒的杰羅斯，拋出問題。

杰羅斯不知何時拿出好幾支肉串到火上烤，周圍蔓延著香氣。

「天曉得？我有太多肉了，根本不記得什麼是什麼。不過你放心，這都可以吃喔？」

肉被火烤過之後，滴出了可口的油脂。這時加上一點鹽和辛香料，再放到火上去烤一下之後，杰羅斯將烤好的肉串交給克莉絲汀。香氣挑動著食欲。

儘管覺得不雅，但克莉絲汀還是忍不住咽下口中的口水。

接著她緩緩地咬了一口肉串。熱騰騰的肉串那香甜的肉汁和柔軟的口感在口中融化擴散，帶來一股濃烈的味覺享受。

「好、好好吃……」

她只說得出這句話。因為從墜落到最下層之後，她什麼也沒吃，這些能填飽肚子的肉串真是最棒的饗宴。

「啊，這是蠍獅、翼龍跟……死亡螳螂的肉。」

「咳噗！咳咳呸呸！」

杰羅斯從入口的肉味想起這些是什麼魔物的肉了。

蠍獅跟翼龍的肉是最高級的食材，那可是連貴族也鮮有機會享用到的夢幻食材。但死亡螳螂就是一種神秘的肉類。更別說這種魔物在這個世界也屬於昆虫類，不是每個人都會想嘗試食用的玩意。而且這三種魔物都是因持有凶惡的力量而聞名的危險生物。

這些肉毫無疑問地不是隨隨便便可以獲得的東西，而死亡螳螂的肉也確實沒有人食用過，除了眼前這個大叔之外……

「你、你怎麼給我吃這種東西！」

「有什麼問題嗎？」

「這都是高級食材耶！而且你剛剛說死亡螳螂？」

「這白色的肉就是了，不覺得肉中帶點甜味，很好吃嗎？」

確實很好吃。若她不知道是什麼的肉，她應該會覺得這是世界上最好吃的肉。

但死亡螳螂對她來說，等於是吃了粗劣的食物。

「雖、雖然是很好吃沒錯……」

「照這樣來看，大蜘蛛或許也很好吃呢。是否該來試一次看看。」

「你要吃那個？怎麼想都覺得這不正常。」

「只是吃魔物的肉啊，有什麼奇怪的嗎？反正都是吃屍體，沒差吧。」

「唔，確實是如此……」

「好吃就是正義。我想在這種情況下還有東西可以吃，就應該感到幸運了吧？」

杰羅斯因為經歷過野外求生的生活，所以成長得非常堅強。雖然主要是強化了野性這方面，但即使世界毀滅，總覺得他一個人也能活得下去。相對的，克莉絲汀覺得手中的肉串看起來簡直像是奇怪的東西。在這之後她雖然煩惱了一下，最終還是敵不過飢餓感，吃下了這串肉串。

甚至還多吃了幾支──果然好吃就是正義啊。

吃過午餐之後，兩人再次以上方樓層為目標前進。途中遭遇了魔物。

「是『戰蟻』啊，要不要試著打倒它？畢竟只有一隻。」

「可是那個的等級……」

「等級103，有你手上那把武器就沒問題了吧。」

杰羅斯指的是借給她的青龍刀。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

魔改青龍刀【三十八式青龍刀改】

加強切割能力的強力名刀。以絕佳比例加入多種超大型魔物的素材後打造而成，超乎常識的玩意。銳利程度只消一碰就能切開鋼鐵，空手碰觸十分危險。

特殊效果

體能強化、切割強化、斬擊強化、一擊必殺、一刀兩斷、攻防合一

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

『這次沒有顯示攻擊力呢，是換了一個負責人嗎？』

非常隨興且鑑定的內容也每次都不同的這個能力，讓杰羅斯不禁抱持奇怪的疑問。

這個世界的法則似乎有些奇怪的傾向。另外，會獲得技能這件事本身感覺上似乎就已經偏離了自然界的法則。杰羅斯不免認為這一定有什麼人在管理。

「總之我會封鎖它的行動，你就趁這個機會進攻。」

「……好、好的，我會努力。」

克莉絲汀變得有些消極。但這也不怪她，畢竟她接連遭遇危險的狀況。但杰羅斯並沒有發現，最大的原因是出在飛行魔法和肉串上。

她從鞘中取出青龍刀，朝戰蟻衝了過去。

「『電漿束縛』。」

杰羅斯懶散地使出單體束縛魔法，戰蟻的身體變因為帶電的雷而麻痺，無法動彈。

克莉絲汀一氣呵成地砍過去。或許因為平時訓練有素的關係，揮劍的動作既標準又漂亮。

「喝呀啊啊啊啊啊啊啊啊啊啊！」

──啪！

戰蟻巨大的頭部從胸口附近被斬下，一命嗚呼。

魔改武器的威力實在太強大了。

「呃，咦咦──────？這威力是怎麼回事？」

那是追加技能「一擊必殺」跟「一刀兩斷」帶來的效果。這麼一來就可以證明魔改武器就算讓低等級的新手來拿，都是一件很危險的事。要是給杰羅斯用上了，真不敢想像威力會去到什麼境界。

「……這武器太危險了呢。離開之後還是封印起來吧，太不妙了……」

「我覺得拿著這把武器的自己很可怕……這是我做的，沒錯吧？」

「之後打造武器的時候還是慎重點吧。要是太得意忘形，真不知道會產出什麼危險物品呢。哎呀～好可怕、好可怕。」

「你是基於半是好玩的心態做出這種威力超乎尋常的武器的嗎？」

「我已經在反省了，老實說真的做得太超過了。不過我並不後悔。」

儘管是在遊戲內，但杰羅斯直到現在才理解到自己究竟做出了多麼凶惡的武器。

即使使用者等級很低，也可以一擊打倒等級超過自身五倍以上的魔物。光是這把武器就足以毀壞這世界的軍事平衡。然後，既然打倒了強大的魔物，等級自然會提升。

「咿呀啊啊啊啊啊啊啊啊啊啊！」

等級三級跳給身體帶來負擔，若等級有巨大差距，身體為了從適應的痛楚之下保護個人意識，會強制陷入睡眠。若只是疲勞那還是小事，一個不小心可能會睡上好幾天都醒不過來。克莉絲汀的狀況屬於後者。無法承受痛楚的身體在開始適應等級變化的同時切斷了意識，只見她緩緩倒下，杰羅斯急忙上前撐住她。

結果，克莉絲汀還是在這坑道迷宮裡失去了意識。

「等級到底差多少啊？」

她原本的等級是20，現在一口氣跳到了81。

集團作戰和個人作戰分配到的經驗值似乎不一樣，若是以集團作戰的方式，經驗值會經過分配，適應時產生的症狀也會受到限制，但克莉絲汀現在則是一口氣產生變化。

實際上在法芙蘭大深綠地帶，包括騎士在內都是一點一點慢慢升級的。

症狀大多是產生一些疲倦感而已，但她現在正受到劇烈痛楚的煎熬。

這麼一來，就只能由杰羅斯負責扛走她了──

「這樣我看起來豈不是很像綁匪嗎？」

若從世俗的眼光來看，只會覺得外表看起來就很可疑的他是個綁架少女的罪犯吧。看起來太糟糕了，而且克莉絲汀還打扮成少年的模樣。

這樣所呈現出的景象雖然會讓許多人看了猛搖頭，但在一部分的女性之間卻極有人氣。

綁架美少年的可疑大叔，給人的觀感實在糟透了。

要是這樣的流言傳了出去，自己大概也不用活了。杰羅斯不禁臉色發青。

「這該怎麼辦是好呢……」

他只剩下兩個方法。前進，或者是在原地等待。

到頭來杰羅斯還是選擇扛著克莉絲汀走，畢竟比起世俗眼光，還是該以人命為優先。

題外話，克莉絲汀胸部意外地挺大的。杰羅斯一邊隱藏住因為偶然得知這事實而產生的各種困惑，一邊冒著將會背負不名譽罪名的危險向前邁進……

◇　◇　◇　◇

「可惡，這些臭螞蟻！」

「別著急。那位魔導士已經去找克莉絲汀小姐了！他們很可能平安無事。」

「話是這麼說沒錯，但從他掉下去到現在已經過了一大段時間！要是在這之間發生了什麼萬一，我們……」

騎士們被巨蟻群擋住了去路。

螞蟻們為了守護巢穴，摩擦著尖銳的下顎發出聲響威嚇騎士們。在不需要確保食物的迷宮裡，它們最重要的生存目的就是繁殖，因此會為了排除進入自身地盤的外敵而變得很有攻擊性。一邊是為了物種延續，一邊則是為了找回侍奉的主人而戰。

礦山外頭應該已經天黑了吧，但他們還是無法離開這邊。

「賽爾！擋住左邊的螞蟻！」

「知道了！但我撐不了多久喔！那邊只有伊札特可以嗎？」

「柯爾薩，跟我一起來收拾右邊的螞蟻！支援就拜託你了。」

「明白。」

「我去支援賽爾。索科塔，快點打倒它們！」

騎士兵分二路，利用地形狹窄之便為了打倒巨蟻而揮劍。

巨蟻被狹小的通路卡住身體，四位騎士抓準時機一口氣殺上去。

他們雖然很疲憊，還是必須救出克莉絲汀。已故領主愛德華將克莉絲汀托付給他們，克莉絲汀是他們發誓絕對要守護的艾維爾家繼承人。

這些騎士都是孤兒，他們知道若不是已故的愛德華收留，自己現在想必不是什麼正當的人吧。在窮困至極的貧民區長大的他們，都認為收留並養育自己的愛德華是恩人。

所以才會為了已故領主留下的克莉絲汀如此拚命。

「啊啊～找到了！螞蟻正群聚在一起。叔叔們，我來幫忙吧。『噴射水柱』。」

從後頭趕上的伊莉絲等人與騎士會合，戰況變得輕鬆許多。

「真是的，希望你們可以冷靜點行動啊！」

「都有杰羅斯先生在了，不會有問題啦！別說這個了，不集中在眼前的狀況會有危險喔？」

嘉內以大劍砍倒巨蟻，雷娜則以迅速的動作集中攻擊關節。

「感謝相助！」

「碰上這種事情就是要互相幫忙吧？這可是傭兵的常識啊。」

雖然有些人不適用這個常識，但伊莉絲等人可是正經的傭兵。

「開口之前先動手！又從裡面出來嘍。『冰風暴』。」

零下三十度的冰冷風暴漸漸凍住成群結隊的巨大螞蟻，騎士與嘉內等人隨後勇猛衝刺將之粉碎。

「數量真多，到底有多少啊。」

「若這裡真的是迷宮，魔物會繁殖到多不勝數的地步喔。」

「真不想面對，但我們非得向前邁進不可。」

騎士們急了起來。前方是成群結隊的螞蟻集團，但不前進就無法救出克莉絲汀。然而現實就是他們正被障礙阻撓了去路。

「騎士先生們，冷靜點。喏，聽得到吧？」

「聽得到……什麼？」

「等等，螞蟻們的樣子有點怪！」

巨蟻開始慌張地戒備周遭，忙碌地動著觸角、下顎咬合發出聲響，正在彼此溝通。

不管怎麼看，都覺得它們也急了，應該是發生了出乎意料的狀況。

──轟隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆！

足以撼動整座坑道的巨響回蕩在坑道裡。

同時，在狹小空間中加速的旋風彷佛要吹走它們般襲擊而來。

巨蟻一口氣逃進另一條坑道，從騎士們面前消失了。

「发、發生了什麼事……」

「方才礦山劇烈震動的原因也不明，該不會是要崩塌了吧？」

「別說這種不吉利的話，這樣我們不就逃不掉了！」

「喂，有人從裡面出來了……是那個魔導士！」

穿著灰色法袍，看起來就很可疑的中年大叔正從粉塵中緩緩走出。

背上背著騎士們打算拯救的少女。

「克莉絲汀小姐！」

「哎呀？大伙全都在這裡啊。話說，往出口的路走這邊對嗎？我只是隨便往上爬而已。」

「別說這個了，克莉絲汀小姐沒事嗎！」

「沒事喔，只是因為升了些等級昏過去罷了。」

杰羅斯把克莉絲汀交給騎士們，總算能休息一下了。

在他的周圍，雷娜和嘉內正在支解巨蟻，伊莉絲則回收了被迷宮吸收後殘留下來的巨蟻魔石。

素材必須在被迷宮吸收之前回收，所以可以理解支解工作必須跟時間賽跑，但完全沒人擔心杰羅斯的安危。甚至還埋首於工作之中，簡直把他當空氣。

有點傷心的中年大叔覺得被排擠了，開外掛的大叔注定孤單。

大叔在心中感覺到一陣冷風吹過，走出了礦山。

一邊聽著背後三個少女開心聊天的聲音……