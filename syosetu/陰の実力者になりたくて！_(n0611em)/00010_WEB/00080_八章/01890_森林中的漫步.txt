  對於那個名叫米莉亞的少女，艾普西隆心中有著些許線索。


  那是在主人上學之前所發生的事。當時的影守規模還很小，只有主人與七陰進行著活動。

  當時，以主人的姐姐克蕾亞被迪亞波羅斯教團抓走為契機，他們摧毀了一個教團的支部。


  被任命為那個支部的管理者的教團幹部的名字是歐爾巴。然後在歐爾巴的短劍上所刻有著的女兒的名字就是米莉亞。

  繼續調查了之後，判明了歐爾巴的女兒米莉亞被挾持為人質的事實。


  後來、米莉亞也被發現了。

  但是她由於教團的實驗而變成了怪物，應該已經被阿爾法處理掉了才對。

  那具遺體是被教團回收了嗎。

「你們這些傢伙拿到沒有一絲人性嗎……」

  艾普西隆瞪向浮現出輕蔑笑容的都艾姆。

「雖然說讓你親自來處理我有些不好意思，但是這樣一來形勢就逆轉了呢。『七陰』的首級，就留在這裡吧」

  從破碎的棺材中出現的，是美麗的少女。

  尚還殘留著一絲天真爛漫的眼瞳，以及灰色的頭髮，讓人感受到了曾經一度見過的歐爾巴的面影。

  都艾姆以米莉亞為盾後退著。

「……」

  艾普西隆也在警戒著米莉亞。

  從米莉亞的體內，感受到了足以匹敵英雄芙蕾雅的亡靈的魔力。

  帳篷外的騷動正逐漸接近。

  希和奧米伽的佯攻好像也差不多要到極限了。這裡應該也很快就會被包圍吧。

  ——是時候撤退了。

  雖然會讓都艾姆逃掉，但是教團的戰力已經確實的被削弱了。最重要的事，知道了教團的計劃這點非常重要。

  被感情所推動，做出錯誤的判斷是非常愚蠢的。

  艾普西隆用其緻密的控制力，將魔力集中在劍上。


  而就這麼一瞬之間，米莉亞便對此做出了反應。

  但是，艾普西隆的目標並不是她。

  而是在其身後的——都艾姆。

  艾普西隆揮出劍來。

  被集中起來的魔力，化作了利刃向都艾姆放出。

「什！？米莉亞，快保護我！」

  正要襲向艾普西隆的米莉亞，對都艾姆的聲音做出反應、改變了前進的方向。

  然後在進入利刃的射程中之後，放出了巨大的魔力將利刃打消了。


  艾普西隆在面具之後微微睜大了眼睛。

  沒想到會以這種方式被打消呢。

  但是，目的已經達到了。

  艾普西隆從帳篷之上跳出，向著森林跑去。

「追上去米莉亞！絕對不能放過她！！」

  能夠感覺得到巨大的魔力氣息從背後追趕而來。


◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇


  雖然我計劃了影之精銳部隊的強襲作戰，不過與艾普西隆她們分開之後，我正全力在森林中衝刺時紫羅蘭小姐的手指起了反應。

  她在口袋之中摸索摸索的扭動著。

「等下、好癢啊」

  嘛、沒有辦法我只得停止了腳步，將紫羅蘭小姐取了出來。

「什麼、怎麼了嗎？」

  在我的手掌之上、紫羅蘭小姐啪嗒啪嗒的動著。

「肚子餓了嗎？」

  啪嘰啪嘰、手掌被她打了。

「不對嗎。啊、是想要上廁所嗎？」

  啪嗒啪嗒，手掌被打了。

「嗯、真是為難啊」

  我試著找了找周圍有什麼東西。

  只是普通的森林而已。

  但是天空之中漂浮著美麗的月亮。

「月色真美呢。但是因為現在很忙所以以後再一起去看吧」

  紫羅蘭小姐的手指摸索摸索的扭動著。

  肢體語言實在是很難懂啊。

  她從我的手掌上掉落下來，咕嚕咕嚕地在地面上滾動著。

「是要去散步嗎？」

  我跟在了她後面。

  走了一會兒、森林中聚集起了淡淡的光芒與魔力。

  那個一點點的變強之後、化作一個女性的身姿。

「哎呀、又見面了呢」

  我像這樣對芙蕾雅小姐打了個招呼。
