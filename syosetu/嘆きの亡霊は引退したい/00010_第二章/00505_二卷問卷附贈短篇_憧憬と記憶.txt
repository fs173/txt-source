即使是雙手空空只抱著夢想從鄉下來到帝都的孩子，做了好幾年的寶物獵人後，自然也會有個人物品累積了起來。

隔了好久整理一次桌子的抽屜，冒出了各種各樣的東西。
有過了使用期限的魔法藥水，又有成為獵人後在帝都第一次購物買來的稍微高檔的多用途小刀。有第一次吃巧克力時驚訝於其美味而留下的包裝紙，還有被太沉重的工作折斷了心弦（不如說人家的心弦是現在進行式的被折斷中），而認真猶豫辭去獵人職務後，應該從事什麼工作時所購買的招聘雜誌。有的物品能鮮明地憶起購買時的事情，亦有的物品已經一點也記憶也沒有了。

想起來，我做得可真不錯。試著回想成為獵人之後發生的事情，雖然已經到了發生太多事而無法全部回想起來的水平，但我仍然像這樣子的平安地活到了今天。

一邊沉浸在鄉愁中，一邊繼續打掃的時候，從抽屜深處發現了一本獵人初學者用的指南書。
那不是在帝都買的，而是在故鄉的小鎮上決定以獵人為目標的時候，和幼馴染們一起購買的東西。
經過日晒的茶色封面，還有被磨得皺巴巴的內頁。之所以到處都殘留著血漬和正體不明的污點，是因為我曾經有一陣子隨身攜帶這本書。

找到了令人懷念的東西呢⋯⋯⋯我隨意地翻閱著書。

內容是寶物獵人的基礎中的基礎。作為獵人相關書藉非常有名，現在也仍然作為其中一冊寶物獵人入門書而廣為人知。
雖然現在終究已經沒有必要了，但對於當時的我們來說，這本書的內容正等同獵人這種職業。

使用這本指南的時候 我還是很認真的，看到裡頭的空白位置近乎都被寫上了筆記，就能看出我當時的熱情。

這時，突然從書頁的縫隙中滑落了不知什麼。

屈身拾起，舉向光源察看。
指南中夾著的，是一張照片。拍照的日期應該是五年前──剛來到帝都不久的時候，以探索者協會的支部為背景拍攝的。雖然從年月上來說應該沒過多久，但卻覺得這是很久以前的事了。
當時的我們才１５歲──剛剛成年，才剛成為寶物獵人，即使有知識也沒有經驗，是上下左右都分不清的時候。
儘管如此，照片中的我們還是露出一副很開心的表情。即使是唯一皺著眉頭的過去的我，和現在的我相比，掛著的表情還是有霸氣得多。那時的我還打算竭盡全力完成隊長的職務，能看出拚命的決心。

照片中的是，除去當時還未加入隊伍的艾麗莎，年輕的《嘆息的亡靈》的成員、六人和── 一名黑髮的小女孩。小女孩張著黑溜溜的眼睛，露出緊張的表情。從外表來看，她明顯不是獵人。

為什麼會有外人在紀念照之中呢？我一瞬間歪了下頭，馬上想到了是什麼一回事，捶了一下掌心。

這時，正好傳來了敲門的聲音。

「MASTER，失禮了。嘉庫支部長⋯⋯說想找MASTER幫忙⋯⋯」

出現的是緹諾。黰黑的秀髮、左右繫著紅色緞帶。與師傅相比裸露較少的黑色戰鬥服非常適合她。

照片中的小女孩和她的身影重疊在一起。
不就是提諾嗎。和現在相比，眼神很柔軟，頭髮也很短。個子雖矮得多，卻有著一下子就讓人聯想到的面容。

嗯⋯⋯我沒有忘記。我才沒有忘記唷？

當時的緹諾不是獵人──不如說，根本就還未成年，但因為來到帝都之後很快就認識了，所以在照片中也並不奇怪。

⋯⋯不不不，為什麼會照進去的？一般來說隊伍的紀念照中可沒有外人進入的餘地。

向停下整理的手，皺起眉頭的我，帶來麻煩事的緹諾戰戰兢兢地走了過來。

「MASTER⋯⋯那個⋯⋯嘉庫支部長說希望您趕緊過去──」

我把照片遞到了她的眼前。

「⋯⋯緹諾，你還記得這個嗎？」

「！！⋯⋯是的，當然記得！」

緹諾睜大眼睛凝視著照片。你看，果然就是緹諾。
她的眼眸閃閃發光。好像對事情有記憶。雖然我完全忘記了照片的存在，不過，說不定對她來說是美好的回憶。

⋯⋯不是啦，我不是薄情，只是因為那時候每天都拼得要死⋯⋯⋯

「是MASTER⋯⋯讓在旁邊目不轉睛地看著的我，加進來的！」

完全不記得了。為什麼我會做出那種事呢。
對掛上半吊子的笑臉的我，緹諾罕見地感動至極的繼續說道。平時酷酷的緹諾除了關於姐姐大人以外的事，很少會把感情暴露到這種地步。

「總有一天⋯⋯當我成為了一流獵人的時候，就會變成寶貴的一張照片，MASTER這樣說服了姐姐大人她們──」

「！！」

聽到這句話，我終於想起當時的我為什麼要讓緹諾加入拍攝紀念照了。
是個單純的理由。因為以前，我還不是獵人的時候，也曾被某位獵人說過完全一樣的話一起拍了照片。那位獵人，其實就是給予我們以獵人作為目標的契機的人──所以，我也想模仿來裝個帥──才讓她加進來拍照的！

受到您如此感激，萬分抱歉，這完全只是對別人的現學現賣而已。

我閉上眼睛微微地咳了一聲後，就封住罪惡感笑了。

「⋯⋯也就是說，我沒看走眼呢」

「怎麼會⋯⋯！MASTER、我這種的──還差得遠了」

緹諾睜圓眼睛，像辯解一樣猛烈左右搖著頭。

沒有沒有，我覺得很了不起唷。
與一直隨波逐流做著無法稱之為獵人的獵人的我不同，緹諾擁有作為獵人的足夠實力⋯⋯這種差別到底是什麼回事呢。

是覺得受到表揚了，自己也得說點什麼才妥當嗎，這幾年間受到了莉茲教育的緹諾用籠罩著熱情的聲線說道。

「但是⋯⋯雖然現在的MASTER也很棒，但是當時的MASTER還身纏一股毅然的氛圍⋯⋯非常棒喲」

那句話再次衝擊了我的腦袋。
我想起了當時的我為什麼要擺出這種瞇眼的表情來裝帥。

並不是什麼大不了的理由。
作為隊長就該很強。被其他獵人小看的話會對同伴造成影響，所以最少光是形式上也要裝作強者！我想對當時的我說「該努力的不是那方面吧」

結果，雖然馬上就因為演技而疲憊不堪，變成只有嘴上冷酷無情的角色，但當時的我認為向周圍的獵人們裝成強者是自己的工作。拍這張照片的時候正好是在那個最如日中天的時期。

回想起來非常害羞。緹諾不知道我的內心，用懷念的聲音說。

「無論是姐姐大人，還是希特莉姐姐，都還很小很可愛⋯⋯安塞姆兄長也⋯⋯這個時候還只有這麼小隻呢⋯⋯」

因為安塞姆的身高開始長高是成為獵人後過了一段時間的事呢⋯⋯儘管如此，照片中的安塞姆和我的身高相差無幾，不過，數年就竟然長高了一倍以上 當時的我們誰都沒預料到吧。

嘛、總之照片的謎團是解開了。當時的我的自命不凡先暫且不論，也許只有那句「總有一天會成為美好回憶」的話語是正確的。那麼說來，這時候的緹諾並沒有系上絲帶呢⋯⋯⋯

明明已經充分看清楚了，緹諾卻好像要印在眼底似的凝視著照片。臉頰泛紅，手微微顫抖。真誇張哩⋯⋯因為有分配給全部人，緹諾明明應該也有同樣的東西的。

大辦公桌的抽屜的整理，還有一半沒做完。
我向呆立不動的緹諾開玩笑的問道。

「難不成、照片、丟了之類的？」

「誒！？⋯⋯那個⋯⋯我沒、沒有、收到喲⋯⋯」

緹諾發出顫抖的聲音。

呃？沒給她嗎？明明還裝帥讓她拍了紀念照？⋯⋯⋯⋯是這樣哦──。

試著搜尋記憶，但畢竟連拍照的事都忘了，所以完全不記得了。但是，即使是只拍了照 但忘記把照片交給對方，也並不奇怪。雖然當時的我一副嚴肅的樣子，但那毫無疑問是我。
即使追溯到幾年前，我還是一如既往沒救的傢伙。

我一邊對過去的自己感到無奈，一邊指向緹諾拿著的照片。

「嗯，那麼，那張照片給你吧。」

反正是連拍了的事都給忘了的照片，比起我，似乎是緹諾會更珍惜它。
對我隨意說出的話，緹諾氣勢洶洶地向我探出身子。

「真、真的嗎、MASTER！？我真的、可、可以收下嗎！？」
「啊、嗯。」
「謝⋯⋯謝謝，非常感謝。MASTER！這麼棒的東西──我絕對、會好好珍惜的」

雖然我因那洶洶的氣勢而震顫，但好象沒看進緹諾的眼裡。
她用袖子抹擦濕潤的眼眸，滿臉喜色地無數次的低頭道謝。明明只是張普通的照片，本來就是打算交給提諾的。她那麼高興，我反而開始覺得不好意思。

對高興得連耳朵都一片嫣紅的緹諾，我稍微帶點關懷的說道。

「好啦，因為緹諾也變優秀（註）了⋯⋯對了。下次再拍照吧。你看，我想這會成為一個很好的紀念⋯⋯」（註：原文「立派」，可以解外表的「漂亮」/能力上的「出色」）

「！！是、是！務必⋯⋯如果、我可以的話，請務必讓我一起。MASTER！」

對用全身表現出喜悅的緹諾，我想起了從前的她，因懷念而象照片中的我一樣地眯起了眼。

⋯⋯這麼說來，緹諾這孩子⋯⋯好像完全忘記了來叫我的理由。被嘉庫桑叫去啥的，反正是我沒有義理去接受的麻煩事吧。就當沒發生過吧。

§　§　§

帝都塞布露迪亞是獵人的聖地。城市通過優待寶物獵人而達到如此的繁榮。
但是，對於住在這個城市的居民來說，獵人是帶來財富的存在，同時也是擁有人外力量的畏懼對象。

當然，大部分的獵人都是奉公守法的正直的人。但是，也存在極少一部分人會毫不猶豫地向無辜的百姓施展其所得到的力量。然後，對於被襲擊的人來說，對犯法、施行暴力毫不猶豫的深紅獵人，在獵人中不過只佔極少一部分的事實，並沒法成為令他們安心的材料。

在帝都只有極少數，但是會定期發生的，擁有力量的獵人的犯罪一直成了很大的問題。

在帝都出生長大，一直注意不要和獵人有所牽連的緹諾會被選中作為目標，也應該只是不幸的事故。

動機是「誰都可以」。嬌小的，軟弱得無法反抗的，只要是女性的話就可以。
對方是剛到達等級３，獵人落伍者的，男性二人組。雖然在獵人中是沒什麼了不起的角色，但對於不涉戰鬥的一般市民來說也是無法抗拒的對手。

緹諾至今還記得。需要抬頭仰望的體格，俯視緹諾的渾濁眼瞳。那因日晒而變黑的手指又粗又硬，雖然沒有拔劍，但那是因為他們認為沒有拔劍的必要吧。
唾沫飛濺怒吼的同時，嘴角醜陋地歪斜著。被牽扯上的原因是什麼呢──好像是被緹諾碰撞了還是什麼，不過，因為腦中一片空白所以想不起來。

緹諾因恐懼而說不出話，身體失去力氣只能後退。當路過的每個人都轉過眼睛的時候，走進逼近的男人們和緹諾之間的，是一群戴著『奇異骸骨』的一伙人。

面對處於強烈的恐懼中再次發生的異常事態，緹諾感覺仿彿在做夢一樣。

然後，即使是變裝也不會使用的吧，那過於不祥的面具下，直接地碰上深紅獵人的纖瘦青年以絲毫感覺不到恐懼的聲音說道。

「抱歉──沒注意到有人在」

──竟然忘記在（面具上）眼睛的位置開洞，真是不小心呢。

一邊開著玩笑一邊摘下面具的那個身姿，緹諾是永遠不會忘記的吧。

這就是緹諾・謝德以獵人為目標的原點。『＜Ruby＞嘆息的＜Rt＞Strange</Rt></Ruby><Ruby>亡靈<Rt>Grief</Rt></Ruby>』和，將來成為其中一人的弟子的緹諾的第一次接觸。
也是她漫長的獵人人生的開始。

這還是那個惡名、勇名在帝都中轟鳴之前的故事。