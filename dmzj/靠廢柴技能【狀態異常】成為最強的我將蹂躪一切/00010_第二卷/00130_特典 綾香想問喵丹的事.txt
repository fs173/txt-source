以女神代理人身分，來到勇者們面前的喵丹・琪琪佩。
她表情冷靜地一腳踹飛自我介紹後突然衝上前來的小山田翔吾。十河綾香戰戰兢兢地對她說：

「請問⋯⋯」
「你是──Ｓ級的綾香・十河。什麼事？」
「你好強喔！」
「因為我是薇希斯之徒。」

薇希斯之徒。似乎是直屬於女神的特務人員稱號。
喵丹邊確認手腕的狀況邊說：

「不夠強的人，無法勝任薇希斯之徒。當然，這個職位也不是光靠戰鬥能力就做得來。」

綾香原本的計畫，是想藉著問題一探喵丹如此強大的秘密。
但是，得到的回答卻不著邊際，連碰都碰不到秘密的邊緣。
喵丹看起來不像愛說話的人。她給人的印象，應該是討厭多說廢話的類型吧。
因為有身為班長的經驗，使綾香養成了分析對方個性的習慣。
就在此時，喵丹的視線轉向綾香。

「⋯⋯你不用我教，應該也會成長吧。我大略一看，就可以知道你是在這裡的異界勇者之中，最擅長戰鬥的人物。」

喵丹將視線拋向2C所有同學。

「我從散發出來的氣氛就曉得，你們是在和平的環境長大的。但是，十河同學你的舉手投足都有戰士的氣質。」
「我從祖母那裡學習過古武術──就是戰鬥的方法。只不過，在我們原本的世界不太需要就是了⋯⋯」
「原來如此。意思是，你學過『實戰式』的戰鬥方法，不過『實戰』的經驗尚淺，是嗎？」

喵丹彷佛看穿了什麼似地，微微眯起眼睛。

「你們心中還殘留不少對這世界各種事物感到的『困惑』。畢竟之前身處於和平的世界，那也是理所當然的。不過──」

喵丹的視線依序輪流掃過桐原拓斗、小山田翔吾、安智弘、戰場淺蔥，以及高雄姊妹。

「你們之中，有一名勇者對於困惑沒什麼感覺⋯⋯不僅如此，應該說從那個人身上完全感受不到困惑，我反倒對那個人比較感興趣。」

勇者中的確有幾個人，對異世界的適應力快到反而令人覺得不自然。

「我還有一個問題，就是⋯⋯你那套服裝⋯⋯」
「？」
「是你自己選的嗎？」

綾香指的是喵丹身上穿的服裝。可以說，非常暴露。
喵丹似乎無法理解綾香發問的用意，她反問：

「有什麼問題嗎？」
「呃⋯⋯我只是覺得很暴露⋯⋯」

綾香覺得那套服裝穿在個性冷酷的她身上，總覺得不太平衡。

另一方面，不得不說那套衣服很適合她，卻也是事實。
她一身裝扮有種性感蠱惑的氣氛，觀察一部分男生的視線和嘴角，就能知道那身打扮對異性的刺激非常強烈而深具魅力。
只不過，對討厭暴露身體的綾香而言，實在很想碎念一下。

喵丹看著男生們。

「你就那麼在乎異性的目光嗎？」
「咦？」
「就算他人用邪佞的眼光打量自己，也不會有什麼損失。再說，假設真的有人心懷邪念，上前攻擊，我也能制伏他⋯⋯即使不使用暴力，我也有方法能讓對方無力化。只要變得夠強，大部分的事情自然都不成問題。」
「可、可是⋯⋯就算如此，有必要穿那種服裝嗎？」

綾香的問題像是在問她「穿這麼少必定有理由」，但其實綾香打從一開始就認定，這名叫做喵丹的人物，一定因為這套服裝，遭受過某種傷害與損失。

喵丹面無表情地盯著綾香看了好一會兒。
然後，輕輕嘆了一口氣。

「這套服裝和裝備，是薇希斯大人賜給我的。」
「啊⋯⋯」
「因為身為『薇希斯之徒』，不能拒絶穿上女神賞賜的衣服。」